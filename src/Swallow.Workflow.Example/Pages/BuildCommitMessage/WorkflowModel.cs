﻿namespace Swallow.Workflow.Example.Pages.BuildCommitMessage;

public enum Risk { KnownSafe, Validated, Risky, Broken }
public enum Intention { Feature, Bugfix, Refactoring, Documentation }

public sealed class WorkflowModel
{
    public string? Title { get; set; }
    public string? Description { get; set; }
    public Risk? RiskLevel { get; set; }
    public Intention? Intention { get; set; }
}
