﻿namespace Swallow.Workflow.Example.Pages.BuildCommitMessage;

public static class CommitMessageGenerator
{
    public static string Generate(Intention intention, Risk risk, string title)
    {
        var letter = intention switch
        {
            Intention.Feature => "F",
            Intention.Bugfix => "B",
            Intention.Refactoring => "R",
            Intention.Documentation => "D",
            _ => throw new ArgumentOutOfRangeException()
        };

        var prefix = risk switch
        {
            Risk.KnownSafe => letter.ToLower(),
            Risk.Validated => letter,
            Risk.Risky => letter + "!!",
            Risk.Broken => letter + "**",
            _ => throw new ArgumentOutOfRangeException()
        };

        return $"{prefix} {title}";
    }
}
