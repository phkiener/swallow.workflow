﻿namespace Swallow.Workflow;

using Blazored.SessionStorage;
using Example;
using Microsoft.Extensions.DependencyInjection;
using Static;

public static class ServiceProviderConfig
{
    public static IServiceCollection AddWorkflows(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<TempDataProvider>();
        serviceCollection.AddScoped<UrlGenerator>();

        serviceCollection.AddDependencies();
        return serviceCollection;
    }

    private static void AddDependencies(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddHttpContextAccessor();
        serviceCollection.AddBlazoredSessionStorage();
    }
}
