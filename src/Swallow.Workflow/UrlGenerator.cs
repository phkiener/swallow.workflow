﻿namespace Swallow.Workflow.Example;

using System.Reflection;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;

internal class UrlGenerator(TemplateBinderFactory factory)
{
    public string RouteTo<TPage>(object? routeValues = null) where TPage : ComponentBase
    {
        var routeAttribute = typeof(TPage).GetCustomAttribute<RouteAttribute>();
        if (routeAttribute is null)
        {
            throw new InvalidOperationException($"Cannot build route to {typeof(TPage).Name}: {nameof(RouteAttribute)} is not present.");
        }

        var parsedRoute = TemplateParser.Parse(routeAttribute.Template);

        var routeValuesDictionary = new RouteValueDictionary(routeValues);
        var templateBinder = factory.Create(parsedRoute, new());

        return templateBinder.BindValues(routeValuesDictionary) ?? throw new ArgumentException($"Failed to bind parameters for route {parsedRoute.TemplateText}.");
    }
}
