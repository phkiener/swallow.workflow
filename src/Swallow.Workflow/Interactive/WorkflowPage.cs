﻿namespace Swallow.Workflow.Interactive;

using Blazored.SessionStorage;
using Example;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

[RenderedInteractively]
public class WorkflowPage<TModel> : ComponentBase where TModel : class, new()
{
    [Inject]
    private NavigationManager NavigationManager { get; set; } = null!;

    [Inject]
    private UrlGenerator UrlGenerator { get; set; } = null!;

    [Inject]
    private ISessionStorageService SessionStorage { get; set; } = null!;

    protected TModel Model { get; set; } = new();

    protected override async Task OnInitializedAsync()
    {
        Model = await SessionStorage.GetItemAsync<TModel>("workflow-model");
    }

    protected async Task MoveToAsync<TPage>() where TPage : WorkflowPage<TModel>
    {
        await SessionStorage.SetItemAsync("workflow-model", Model);

        var route = UrlGenerator.RouteTo<TPage>();
        NavigationManager.NavigateTo(route);
    }
}

sealed file class RenderedInteractivelyAttribute : RenderModeAttribute
{
    public override IComponentRenderMode Mode => new InteractiveServerRenderMode(prerender: false);
}
