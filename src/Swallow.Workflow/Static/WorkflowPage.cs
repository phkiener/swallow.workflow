﻿namespace Swallow.Workflow.Static;

using Example;
using Microsoft.AspNetCore.Components;

public class WorkflowPage<TModel> : ComponentBase where TModel : class
{
    [Inject]
    private NavigationManager NavigationManager { get; set; } = null!;

    [Inject]
    private UrlGenerator UrlGenerator { get; set; } = null!;

    [Inject]
    private TempDataProvider TempDataProvider { get; set; } = null!;

    [SupplyParameterFromForm]
    protected TModel? Model { get; set; }

    protected override void OnInitialized()
    {
        Model ??= TempDataProvider.Get<TModel>("workflow-model");
    }

    protected void MoveTo<TPage>() where TPage : WorkflowPage<TModel>
    {
        TempDataProvider.Set("workflow-model", Model);

        var route = UrlGenerator.RouteTo<TPage>();
        NavigationManager.NavigateTo(route);
    }
}
