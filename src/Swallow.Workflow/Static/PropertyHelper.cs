﻿namespace Swallow.Workflow.Static;

using System.Runtime.CompilerServices;
using System.Text.Json;

internal static class PropertyHelper
{
    public static IEnumerable<(string Name, string Value)> EnumerateProperties<TModel>(
        TModel model,
        [CallerArgumentExpression(nameof(model))] string name = default!)
    {
        var propertyQueue = new Queue<(string Name, JsonElement Element)>();

        var serializedModel = JsonSerializer.SerializeToElement(model);
        foreach (var rootProperty in serializedModel.EnumerateObject())
        {
            propertyQueue.Enqueue(($"{name}.{rootProperty.Name}", rootProperty.Value));
        }

        while (propertyQueue.TryDequeue(out var property))
        {
            if (property.Element.ValueKind is JsonValueKind.Object)
            {
                foreach (var member in property.Element.EnumerateObject())
                {
                    propertyQueue.Enqueue(($"{property.Name}.{member.Name}", member.Value));
                }
            }
            else if (property.Element.ValueKind is JsonValueKind.Array)
            {
                var index = 0;
                foreach (var member in property.Element.EnumerateArray())
                {
                    propertyQueue.Enqueue(($"{property.Name}[{index++}]", member));
                }
            }
            else
            {
                yield return (property.Name, property.Element.ToString());
            }
        }
    }
}
