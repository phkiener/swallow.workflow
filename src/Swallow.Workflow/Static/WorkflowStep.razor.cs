﻿namespace Swallow.Workflow.Static;

using Microsoft.AspNetCore.Components;

public partial class WorkflowStep<T> where T : class
{
    [Parameter]
    [EditorRequired]
    public RenderFragment ChildContent { get; set; } = null!;

    [Parameter]
    [EditorRequired]
    public EventCallback OnSubmit { get; set; }

    [Parameter]
    [EditorRequired]
    public T Model { get; set; } = null!;
}
