﻿namespace Swallow.Workflow.Static;

using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Http;

internal sealed class TempDataProvider(IHttpContextAccessor httpContextAccessor)
{
    public T? Get<T>(string name)
    {
        var cookie = httpContextAccessor.HttpContext?.Request.Cookies[$"tempdata-{name}"];
        if (cookie is null)
        {
            return default;
        }

        var decoded = Convert.FromBase64String(cookie);
        var deserialized = JsonSerializer.Deserialize<T>(decoded);

        return deserialized;
    }

    public void Set<T>(string name, T value)
    {
        if (httpContextAccessor.HttpContext is null)
        {
            throw new InvalidOperationException("Cannot set tempdata: No HttpContext is present.");
        }

        var serialized = JsonSerializer.Serialize(value);
        var encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(serialized));
        httpContextAccessor.HttpContext.Response.Cookies.Append($"tempdata-{name}", encoded, new() { HttpOnly = true, SameSite = SameSiteMode.Strict, IsEssential = true });
    }
}
