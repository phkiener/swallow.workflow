﻿namespace Swallow.Workflow.InPlace;

using Microsoft.AspNetCore.Components;

public abstract class WorkflowStep<TModel> : ComponentBase where TModel : class
{
    [Parameter]
    public TModel Model { get; set; } = null!;

    [CascadingParameter]
    protected RenderWorkflow<TModel> Workflow { get; set; } = null!;

    protected async Task MoveToAsync<T>() where T : WorkflowStep<TModel>
    {
        await Workflow.MoveToAsync<T>();
    }
}
