﻿namespace Swallow.Workflow;

public sealed record StepChangedEventArgs(Type NextStep, object WorkflowModel);
